#!/usr/bin/python3

# Copyright (C) 2014-2019 Ralf Treinen <treinen@debian.org>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

import datetime, importlib, os
import options, dosehtml, common
conf = importlib.import_module(options.confmodule)

def add_srcpkgs(src_packages,timestamp,scenario,arch,universe):
    with open(common.htmldir(timestamp,scenario['name'])+"/"+arch+".txt") as f:
        for line in f:
            pkg, ver, _, anchor, desc = line.rstrip().split('#')
            if scenario['type'] == 'binary':
                srcpkg = universe.source(pkg)
            elif scenario['type'] in ['source', 'cross']:
                srcpkg = pkg
            else:
                warning('unknown scenario type: ' + scenario['type'])
            src_packages[srcpkg][scenario['name']].add((pkg, ver, anchor, desc))

def build(src_packages,timestamp):
    common.info("building source package pages")
    # gather all source packages from all scenarios
    os.makedirs(conf.locations['htmlroot']+"/src", exist_ok=True)
    for srcpkg, scenario in src_packages.items():
        outpath = "{t}/src/{n}.html".format(t=conf.locations['htmlroot'], n=srcpkg)
        with open(outpath, "w") as f:
            print(dosehtml.html_header, file=f)
            print('<h2>src:'+srcpkg+'</h2>', file=f)
            print('<p><b>Date: {}</b></p>'.format(datetime.datetime.utcfromtimestamp(float(timestamp))), file=f)
            if all(not scenario[s['name']] for s in conf.scenarios):
                print('<p>No issues.</p>', file=f)
                continue
            for s in conf.scenarios:
                if not scenario[s['name']]:
                    continue
                print('<h3>'+s['description']+'</h3>', file=f)
                print('<table><tr><th>package</th><th>summary</th></tr>', file=f)
                for pkg, ver, anchor, desc in scenario[s['name']]:
                    print('<tr><td>{p} (= {v})</td><td>'
                            '<a href="http://qa.debian.org/dose/debcheck/{s}/{t}/packages/{p}.html#{a}">{d}</a>'
                            '</td></tr>'.format(
                                p = pkg,
                                v = ver,
                                s = s['name'],
                                t = timestamp,
                                a = anchor,
                                d = desc), file=f)
                print('</table>', file=f)
            print(dosehtml.html_footer, file=f)
