#!/usr/bin/python

# Copyright (C) 2014-2020 Ralf Treinen <treinen@debian.org>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

verbose=False

locations = {
    'debmirror'   : '/srv/mirrors/debian/dists',
    'cacheroot'   : '/srv/qa.debian.org/data/dose-debcheck/cache',
    'historyroot' : '/srv/qa.debian.org/data/dose-debcheck/history',
    'htmlroot'    : '/srv/qa.debian.org/web/dose/debcheck',
}

# if set, results of running dose-debcheck will be cached, and cached results
# will be used instead of running dose-debcheck (in case they are newer
# than the input files).
cache_debcheck=False

def _get_architectures(archs):
    import os.path as osp
    from debian import deb822

    for dist in archs:
        path = osp.join(locations['debmirror'], dist, 'Release')
        with open(path) as fp:
            release = deb822.Release(fp)
        archs[dist] = [arch for arch in release['Architectures'].split()
                       if arch not in excluded_archs]

excluded_archs = ('sparc','all')
architectures = {
    'testing': None,
    'unstable': None,
}
_get_architectures(architectures)

scenarios = [
    {
        'name'  : 'unstable_main',
        'type'  : 'binary',
        'archs' : architectures['unstable'],
        'fgs'   : [ '{m}/unstable/main/binary-{a}/Packages.xz' ],
        'bgs'   : [],
        'description' : 'Debian unstable (main only)'
    },
    {
        'name'  : 'unstable_contrib+nonfree',
        'type'  : 'binary',
        'archs' : architectures['unstable'],
        'fgs'   : [ '{m}/unstable/contrib/binary-{a}/Packages.xz',
                    '{m}/unstable/non-free/binary-{a}/Packages.xz' ],
        'bgs'   : [ '{m}/unstable/main/binary-{a}/Packages.xz' ],
        'description' : 'Debian unstable (contrib and non-free only)'
    },
    {
        'name'  : 'testing_main',
        'type'  : 'binary',
        'archs' : architectures['testing'],
        'fgs'   : [ '{m}/testing/main/binary-{a}/Packages.xz' ],
        'bgs'   : [],
        'description' : 'Debian testing (main only)'
    },
    {
        'name'  : 'testing_contrib+nonfree',
        'type'  : 'binary',
        'archs' : architectures['testing'],
        'fgs'   : [ '{m}/testing/contrib/binary-{a}/Packages.xz',
                    '{m}/testing/non-free/binary-{a}/Packages.xz' ],
        'bgs'   : [ '{m}/testing/main/binary-{a}/Packages.xz' ],
        'description' : 'Debian testing (contrib and non-free only)'
    },
    {
        'name'  : 'src_unstable_main',
        'type'  : 'source',
        'archs' : architectures['unstable'],
        'src'   : '{m}/unstable/main/source/Sources.xz',
        'bins'  : [ '{m}/unstable/main/binary-{a}/Packages.xz' ],
        'description' : 'Build-dependencies unstable (main only)'
    },
    {
        'name'  : 'src_testing_main',
        'type'  : 'source',
        'archs' : architectures['testing'],
        'src'   : '{m}/testing/main/source/Sources.xz',
        'bins'  : [ '{m}/testing/main/binary-{a}/Packages.xz' ],
        'description' : 'Build-dependencies testing (main only)'
    },
    {
        'name'  : 'cross_unstable_main_amd64',
        'type'  : 'cross',
        'buildarch': 'amd64',
        # FIXME: autogenerate by taking the architectures for which
        #        crossbuild-essential-${host} exists minus the build
        #        architecture minus architectures that run on that
        #        architecture, for example remove i386 for build==amd64
        'archs' : ['arm64', 'armel', 'armhf', 'mips', 'mips64el', 'mipsel', 'ppc64el', 's390x'],
        'src'   : '{m}/unstable/main/source/Sources.xz',
        'bins'  : [ '{m}/unstable/main/binary-{a}/Packages.xz' ],
        'description' : 'Crossbuild-dependencies unstable on amd64 (main only)'
    },
]

# number of runs that are diplayed
slices = 7

# timeslices for the "summary by duration"
hlengths={0:2,1:4,2:8,3:16,4:32,5:64,6:128,7:256,8:512,9:1024}
