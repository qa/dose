#!/usr/bin/python3

# Copyright (C) 2014-2019 Ralf Treinen <treinen@debian.org>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

import time, os, re, importlib
from collections import defaultdict
import options, universes, reports, horizontal, vertical, cleanup, common, diffs, weather, bts, srcpkgs
conf = importlib.import_module(options.confmodule)

time_now = int(time.time())
day_now  = common.days_since_epoch(time_now)
timestamp_now = str(time_now)

if os.path.exists(conf.locations['cacheroot']):
    timestamps_known = [t for t in os.listdir(conf.locations['cacheroot'])
                        if re.match(r'^[0-9]+$', t)]
else:
    timestamps_known = []
    os.makedirs(conf.locations['cacheroot'])
timestamps_known.sort(reverse=True)
if timestamps_known:
    timestamp_last = timestamps_known[0]
else:
    timestamp_last = None

timestamps_keep = timestamps_known[0:conf.slices-1]
timestamps_keep[0:0] = [timestamp_now]

bugtable=bts.Bugtable()
if options.dump_bugtable:
    bugtable.dump()

src_packages = defaultdict(lambda: defaultdict(set))

for scenario in conf.scenarios:
    summary = horizontal.Summary(scenario,timestamp_now)

    if scenario['type'] in ['source', 'cross']:
        sources=universes.getsources(scenario['src'].format(
            m=conf.locations['debmirror']))
    for arch in summary.get_architectures():
        universes.build(timestamp_now,scenario,arch)
        if scenario['type'] == 'binary':
            universe=universes.BinUniverse(timestamp_now,scenario,arch,summary)
        elif scenario['type'] == 'source':
            universe=universes.SrcUniverse(timestamp_now,scenario,arch,
                                           summary,sources[0],sources[1])
        elif scenario['type'] == 'cross':
            universe=universes.SrcUniverse(timestamp_now,scenario,arch,
                                           summary,sources[0],sources[1],
                                           build_arch_all=False)
        else:
            warning('unknown scenario type: ' + scenario['type'])
        reports.build(timestamp_now,day_now,universe,scenario,arch,
                      bugtable,summary)
        diffs.build(timestamp_now,timestamp_last,universe,scenario['name'],arch)
        srcpkgs.add_srcpkgs(src_packages,timestamp_now,scenario,arch,universe)
        
    horizontal.build(timestamp_now,day_now,scenario['name'],bugtable,summary)
    for what in ['some','each']:
        diffs.build_multi(timestamp_now,timestamp_last,scenario['name'],
                          what,summary)
        
    weather.build(timestamp_now,scenario['name'],summary)
    vertical.build(timestamps_keep,scenario['name'],summary)

    latestpath = common.htmldir_scenario(scenario['name'])+'/latest'
    if os.path.lexists(latestpath):
        os.unlink(latestpath)
    os.symlink(timestamp_now, latestpath)

    # weather.write_available()    

srcpkgs.build(src_packages,timestamp_now)

cleanup.cleanup(timestamps_keep, timestamps_known, timestamp_now, 
                {sc['name'] for sc in conf.scenarios}, src_packages)
