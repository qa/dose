# Copyright (C) 2014 Ralf Treinen <treinen@debian.org>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

"""
Functions that operate on package universes.
"""

import os, subprocess, gzip, lzma, codecs, importlib, shutil
from common import *
conf = importlib.import_module(options.confmodule)

def openr(filename):

    """
    open a possibly compressed file for reading
    """
    
    if filename[-3:]=='.gz':
        reader = codecs.getreader('utf-8')(gzip.open(filename,mode='r'))
    elif filename[-3:]=='.xz':
        reader = codecs.getreader('utf-8')(lzma.open(filename,mode='r'))
    else:
        reader = open(filename,mode='r')
    return(reader)

def realfilename(filespec,architecture,outdir):

    """
    translates a specification string of a filename (a format) into
    a real file name, taking into account the architecture.
    """

    return(filespec.format(m=conf.locations['debmirror'],a=architecture))
    
def run_debcheck(scenario,arch,outdir):

    """
    run dose-debcheck and store the resulting report
    """

    scenario_name = scenario['name']
    cache_fname = "{t}/{s}_{a}.yaml".format(
            t=conf.locations['cacheroot'],
            s=scenario_name,
            a=arch)
    cache_mtime = 0
    input_mtime = 0
    if os.path.exists(cache_fname):
        cache_mtime = os.path.getmtime(cache_fname)
    if (scenario['type'] == 'binary'):
        invocation = ['dose-debcheck', '-e', '-f', '--latest', '1' ]
        invocation.append('--deb-native-arch='+arch)
        for fg in scenario['fgs']:
            invocation.append('--fg')
            fname = realfilename(fg,arch,outdir)
            invocation.append(fname)
            if os.path.getmtime(fname) > input_mtime:
                input_mtime = os.path.getmtime(fname)
        for bg in scenario['bgs']:
            invocation.append('--bg')
            fname = realfilename(bg,arch,outdir)
            invocation.append(fname)
            if os.path.getmtime(fname) > input_mtime:
                input_mtime = os.path.getmtime(fname)
    elif (scenario['type'] == 'source'):
        invocation = ['dose-builddebcheck', '-e', '-f', '--quiet' ]
        invocation.append('--deb-native-arch='+arch)
        for bg in scenario['bins']:
            fname = realfilename(bg,arch,outdir)
            invocation.append(fname)
            if os.path.getmtime(fname) > input_mtime:
                input_mtime = os.path.getmtime(fname)
        fname = realfilename(scenario['src'],arch,outdir)
        invocation.append(fname)
        if os.path.getmtime(fname) > input_mtime:
            input_mtime = os.path.getmtime(fname)
    elif (scenario['type'] == 'cross'):
        buildarch = scenario['buildarch']
        invocation = ['dose-builddebcheck', '-e', '-f', '--quiet',
                      '--deb-native-arch='+buildarch, '--deb-host-arch='+arch,
                      '--deb-drop-b-d-indep', '--deb-profiles=cross,nocheck']
        for bg in scenario['bins']:
            fname = realfilename(bg,buildarch,outdir)
            invocation.append(fname)
            if os.path.getmtime(fname) > input_mtime:
                input_mtime = os.path.getmtime(fname)
            fname = realfilename(bg,arch,outdir)
            invocation.append(fname)
            if os.path.getmtime(fname) > input_mtime:
                input_mtime = os.path.getmtime(fname)
        fname = realfilename(scenario['src'],arch,outdir)
        invocation.append(fname)
        if os.path.getmtime(fname) > input_mtime:
            input_mtime = os.path.getmtime(fname)
    else:
        warning('unknown scenario type: ' + scenario['type'])

    if not conf.cache_debcheck or cache_mtime < input_mtime:
        # caching disabled or cache out of date: run debcheck
        info('running debcheck for {s} on {a}'.format(a=arch,s=scenario_name))
        try:
            with open(outdir + "/debcheck.out", 'w') as outfile:
                subprocess.call(invocation,stdout=outfile)
        except OSError as exc:
            warning('debcheck for {s} on {a} raised {e}'.format(
                a=arch,s=scenario_name,e=exc.strerror))
        # if caching is enabled we now have to update the cache file
        if conf.cache_debcheck:
            with open(cache_fname, "w") as fdst:
                with open(outdir + "/debcheck.out") as outfile:
                    shutil.copyfileobj(outfile, fdst)
    else:
        # cache enabled and cache up to date: use cache file 
        info('using cached debcheck results for '+
             '{s} on {a}'.format(a=arch,s=scenario_name))
        with open(cache_fname) as fsrc:
            with open(outdir + "/debcheck.out", 'w') as outfile:
                shutil.copyfileobj(fsrc, outfile)

def getsources(filename):
    # scans file named filename for source package stanzas.
    # returns a pair of hashtables, associating to each source package name
    # found the contents of Architecture:, resp Version:, field of the last
    # occurrence. Stanzas with 'Extra-Source-Only: yes' are skipped.
    version_table={}
    archs_table={}
    saved=True
    infile = openr(filename)
    for line in infile:
        if line.startswith('Package:'):
            current_package=line.split()[1]
            saved=False
            current_extra_source_only=False
        elif line.startswith('Architecture:'):
            current_archs={arch for arch in line.split()[1:]}
        elif line.startswith('Extra-Source-Only:'):
            current_extra_source_only=line.endswith('yes\n')
        elif line.startswith('Version:'):
            current_version=line.split()[1]
        elif line.isspace() and not saved and not current_extra_source_only:
            archs_table[current_package]=current_archs
            version_table[current_package]=current_version
            saved=True
    if not saved and not current_extra_source_only:
        archs_table[current_package]=current_archs
        version_table[current_package]=current_version
    infile.close()
    return((version_table,archs_table))

#############################################################################
_arch_matches_cache = dict()

def archmatch(arch, wildcards, build_arch_all=True):
    match_found = False
    for wildcard in wildcards:
        if wildcard == 'any':
            return True
        if build_arch_all and wildcard == 'all':
            return True
        cached = _arch_matches_cache.get((arch, wildcard), None)
        if cached is None:
            # environment must be empty or otherwise the DEB_HOST_ARCH
            # environment variable will influence the result
            ret = subprocess.call(
                ['dpkg-architecture', '-i%s' % wildcard, '-a%s' % arch],
                env={})
            ret = True if ret == 0 else False
            _arch_matches_cache[(arch, wildcard)] = ret
            if ret:
                return True
        elif cached:
            return True
    return False

class SrcUniverse:
    """
    A universe object contains information about the source
    packages that exist in a certain scenario.
    architecture. The list of foreground packages is written to a
    file.
    """

    def __init__(self,timestamp,scenario,architecture,summary,
                 versions_table,archs_table,build_arch_all=True):
        scenario_name=scenario['name']
        info('extracting foreground for {s} on {a}'.format(
                a=architecture,s=scenario_name))
        self.fg_packages={p:v for (p,v) in versions_table.items()
                          if archmatch(architecture,archs_table[p],build_arch_all)}
        summary.set_total(architecture,len(self.fg_packages))
        outdir=cachedir(timestamp,scenario_name,architecture)
        if not os.path.isdir(outdir): os.makedirs(outdir)
        with open(outdir + '/fg-packages', 'w') as outfile:
            for f in self.fg_packages: print(f,file=outfile)

        # collect source package name and version for binary packages
        self.source_packages=dict()
        self.source_version_table=dict()
        for inputspec in scenario['bins']:
            inputpath = inputspec.format(
                m=conf.locations['debmirror'],a=architecture)
            infile = openr(inputpath)
            for line in infile:
                if line.startswith('Package:'):
                    current_package=line.split()[1]
                if line.startswith('Source:'):
                    l=line.split()
                    self.source_packages[current_package]=l[1]
                    if len(l) == 3:
                        # the first and last character are parantheses ()
                        self.source_version_table[current_package]=l[2][1:-1]
            infile.close ()

            
    def is_in_foreground(self,package_name,version):
        '''
        tell whether a package is in the foregound
        '''
        try:
            return(version == self.fg_packages[package_name])
        except KeyError:
            return(False)

    def is_in_foreground_someversion(self,package_name):
        '''
        tell whether a package is in the foregound for any version
        '''
        return(package_name in self.fg_packages.keys())

    def source(self,package):
        '''
        return the source package name pertaining to a binary package
        '''
        return self.source_packages.get(package,package)

    def source_version(self,package):
        '''
        return the source version of package if one was specified in the
        Packages file, or None otherwise (in that case the source version
        is equal to the package version)
        '''
        return self.source_version_table.get(package)

#############################################################################
class BinUniverse:
    """
    A binuniverse is a universe for binary packages. It also stores, for
    any binary package name, a source package name and a source version.
    """
    
    def __init__(self,timestamp,scenario,architecture,summary):
        scenario_name=scenario['name']
        info('extracting foreground for {s} on {a}'.format(
                a=architecture,s=scenario_name))

        self.fg_packages=set()
        number_fg_packages=0
        self.source_packages=dict()
        self.source_version_table=dict()

        for inputspec in scenario['fgs']:
            inputpath = inputspec.format(
                m=conf.locations['debmirror'],a=architecture)
            infile = openr(inputpath)
            for line in infile:
                if line.startswith('Package:'):
                    current_package=line.split()[1]
                    self.fg_packages.add(current_package)
                    number_fg_packages+=1
                if line.startswith('Source:'):
                    l=line.split()
                    self.source_packages[current_package]=l[1]
                    if len(l) == 3:
                        # the first and last character are parantheses ()
                        self.source_version_table[current_package]=l[2][1:-1]
            infile.close ()

        for inputspec in scenario['bgs']:
            inputpath = inputspec.format(
                m=conf.locations['debmirror'],a=architecture)
            infile = openr(inputpath)
            for line in infile:
                if line.startswith('Package:'):
                    current_package=line.split()[1]
                if line.startswith('Source:'):
                    l=line.split()
                    self.source_packages[current_package]=l[1]
                    if len(l) == 3:
                        # the first and last character are parantheses ()
                        self.source_version_table[current_package]=l[2][1:-1]
            infile.close ()

        outdir=cachedir(timestamp,scenario_name,architecture)
        if not os.path.isdir(outdir): os.makedirs(outdir)
        with open(outdir + '/fg-packages', 'w') as outfile:
            for f in self.fg_packages: print(f,file=outfile)

        summary.set_total(architecture,number_fg_packages)

    def is_in_foreground(self,package_name,version):
        '''
        tell whether a package is in the foregound
        '''
        return(package_name in self.fg_packages)

    def is_in_foreground_someversion(self,package_name):
        '''
        tell whether a package is in the foregound for any version
        '''
        return(package_name in self.fg_packages)

    def source(self,package):
        '''
        return the source package name pertaining to a binary package
        '''
        return self.source_packages.get(package,package)

    def source_version(self,package):
        '''
        return the source version of package if one was specified in the
        Packages file, or None otherwise (in that case the source version
        is equal to the package version)
        '''
        return self.source_version_table.get(package)

##########################################################################
# top level

def build(timestamp,scenario,architecture):
    outdir=cachedir(timestamp,scenario['name'],architecture)
    if not os.path.isdir(outdir): os.makedirs(outdir)
    run_debcheck(scenario,architecture,outdir)
